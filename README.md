# Audio processing study

This repository aims to collect information about audio models in AI.

## Useful links

- Audio preprocessing library: [librosa](https://github.com/librosa/librosa)
- Article series describing audio in ML (2021): [towardsdatascience](https://towardsdatascience.com/audio-deep-learning-made-simple-part-1-state-of-the-art-techniques-da1d3dff2504)
- Review of Large Audio Models (2023): [arXiv](https://arxiv.org/abs/2308.12792), [GitHub](https://github.com/EmulationAI/awesome-large-audio-models)
- AnyGPT: Unified Multimodal LLM with Discrete Sequence Modeling, [arXiv](https://arxiv.org/abs/2402.12226), [GitHub](https://junzhan2000.github.io/AnyGPT.github.io/)

## Examples

Check out the examples available in the [notebook](./notebooks/) folder.
